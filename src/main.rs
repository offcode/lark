use bevy::{
    prelude::*,
    window::{WindowMode, WindowTheme},
};
use lark::UiPlugin;

fn main() {
    let mut app = App::new();
    app.add_plugins(DefaultPlugins.set(WindowPlugin {
        primary_window: Some(Window {
            mode: WindowMode::Windowed,
            decorations: true,
            resolution: (1280.0, 720.0).into(),
            skip_taskbar: true,
            window_theme: Some(WindowTheme::Dark),
            ..Default::default()
        }),
        ..Default::default()
    }));
    app.add_plugins(UiPlugin {});
    app.run();
}
