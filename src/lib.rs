mod resources;
mod ui;

use bevy::prelude::*;
use bevy_egui::{
    egui::{Align, Button, Color32, Frame, Layout, SidePanel, Vec2},
    EguiContexts, EguiPlugin,
};
use resources::UiState;

pub struct UiPlugin {}

impl Plugin for UiPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins(EguiPlugin)
            .init_resource::<UiState>()
            .add_systems(Startup, setup_ui)
            .add_systems(
                Update,
                (left_nav_ui.before(left_chat_list_ui), chat_main_ui),
            );
    }
}

fn setup_ui(mut _context: EguiContexts) {}

fn left_nav_ui(mut context: EguiContexts) {
    let ctx = context.ctx_mut();
    SidePanel::left("left_nav_ui")
        .exact_width(50.0)
        .frame(Frame {
            fill: Color32::BLACK,
            ..Default::default()
        })
        .resizable(true)
        .show(ctx, |ui| {
            ui.vertical(|ui| {
                ui.horizontal(|ui| {
                    ui.add_space(5.0);
                    ui.with_layout(Layout::top_down(Align::Center), |ui| {
                        ui.add(
                            Button::new("X")
                                .fill(Color32::from_rgb(255, 148, 0))
                                .rounding(25.0)
                                .min_size(Vec2::new(25.0, 25.0)),
                        )
                        .clicked()
                    });
                });

                ui.add_space(5.0);
            })
        });
}

fn left_chat_list_ui(mut context: EguiContexts) {
    let ctx = context.ctx_mut();
    SidePanel::left("left_chat_list_ui")
        .exact_width(50.0)
        .frame(Frame {
            fill: Color32::BLACK,
            ..Default::default()
        })
        .resizable(true)
        .show(ctx, |ui| {
            ui.vertical(|ui| {
                ui.horizontal(|ui| {
                    ui.add_space(5.0);
                });

                ui.add_space(5.0);
            })
        });
}

fn chat_main_ui(mut context: EguiContexts) {
    let ctx = context.ctx_mut();
    SidePanel::left("chat_main_ui")
        .exact_width(50.0)
        .frame(Frame {
            fill: Color32::BLACK,
            ..Default::default()
        })
        .resizable(true)
        .show(ctx, |ui| {
            ui.vertical(|ui| {
                ui.horizontal(|ui| {
                    ui.add_space(5.0);
                });

                ui.add_space(5.0);
            })
        });
}
