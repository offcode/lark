use bevy::prelude::Resource;

#[allow(unused)]
#[derive(Debug, Resource, Default)]
pub(crate) struct UiState {
    pub nav_width: f32,
}
